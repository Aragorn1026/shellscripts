#!/bin/bash
# This shellscript can only run on red hat or centos

if [ $# -lt 1 ];then
	echo "Usage: ./curl.sh url1 [url2 [url3 ...]]"
	exit 1
fi

if [ ! $(echo "1 + 1" | bc) ];then
	echo "There is no bc calculator on your machine!"
	echo "You can install bc by 'yum install bc -y' on redhat/centos or 'apt install bc -y' on debian"
	exit 2
fi

echo '
\n
              http_code:  %{http_code}\n
           content_type:  %{content_type}\n
        time_namelookup:  %{time_namelookup}\n
           time_connect:  %{time_connect}\n
        time_appconnect:  %{time_appconnect}\n
       time_pretransfer:  %{time_pretransfer}\n
          time_redirect:  %{time_redirect}\n
     time_starttransfer:  %{time_starttransfer}\n
         speed_download:  %{speed_download}\n
           speed_upload:  %{speed_upload}\n
          size_download:  %{size_download}\n
            size_upload:  %{size_upload}\n
                ----------\n
             time_total:  %{time_total}\n
\n
' > time_cost.txt

timecost_file=./time_cost.txt
if [ ! -f "$timecost_file" ];then
	echo "The time_cost.txt does not exist!"
	exit 3
fi

for url in $@
do
	echo "curl -I $url"
	result=$(curl -w @${timecost_file} -I $url 2>/dev/null)
	if [ $? -ne 0 ];then
		echo "Url invalid : $url "
		echo
		continue
	fi

        time_namelookup=$(echo -e "$result" | grep time_namelookup | awk -F ':  ' '{print $2}')
        time_connect=$(echo -e "$result" | grep time_connect | awk -F ':  ' '{print $2}')
        time_starttransfer=$(echo -e "$result" | grep time_starttransfer | awk -F ':  ' '{print $2}')
        time_pretransfer=$(echo -e "$result" | grep time_pretransfer | awk -F ':  ' '{print $2}')
        time_total=$(echo -e "$result" | grep time_total | awk -F ':  ' '{print $2}')
        
        nslookup_time=$(echo "$time_namelookup * 1000" | bc)
        tcp_time=$(echo "($time_connect - $time_namelookup) * 1000" | bc)
        server_time=$(echo "($time_starttransfer - $time_pretransfer) * 1000" | bc)
        content_time=$(echo "($time_total - $time_starttransfer) * 1000" | bc)
        total_time=$(echo "$time_total * 1000" | bc)
        
	echo "Nameserver lookup time: ${nslookup_time}ms"
        echo "TCP connection    time: ${tcp_time}ms"
        echo "Server cost       time: ${server_time}ms"
        echo "Url total cost    time: ${total_time}ms"
	echo
done

rm -f $timecost_file
