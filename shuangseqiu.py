
path = 'D:\\tmp\\shuangseqiu.record'
file = open(path)
statistics = open('D:\\tmp\\statistics.txt','w')

line = file.readline()
index = 1
count = 0
liststr = []
fenxi = {'name':'zcq'}

# 数据整理，整理出历次开奖记录，并写到文件、字典中
while line:
    number = line.rsplit('">',1)[1].split('<')[0]
    index += 1
    liststr.append(number)
    if index == 9:
        index = 1
        statistics.write(str(liststr) + '\n')
        fenxi[liststr[0]] = liststr[1:]
        liststr = []
        count += 1

    line = file.readline()

fenxi.pop('name')
# print(fenxi)
file.close()
statistics.close()

# redByNumber和blueByNumber用来统计红蓝球数字出现次数
redByNumber = {'1':0}
for i in range(1,34):
    redByNumber[str(i)] = 0

# print(redByNumber)

blueByNumber = {'1':0}
for i in range(1,17):
    blueByNumber[str(i)] = 0

# print(blueByNumber)

# 红蓝单球走势
red1 = []
red2 = []
red3 = []
red4 = []
red5 = []
red6 = []
blue = []
for value in fenxi.values():
    red1.append(value[0])
    red2.append(value[1])
    red3.append(value[2])
    red4.append(value[3])
    red5.append(value[4])
    red6.append(value[5])
    blue.append(value[-1])
    # 红蓝数字整体出现次数
    for item in value[1:7]:
        redByNumber[str(item)] += 1
    blueByNumber[str(value[-1])] += 1

# 打印次数统计
print("双色球至今红色单球出现次数：")
for key in redByNumber:
    print("数字：" + key + "，次数：" + str(redByNumber[key]))
print("双色球至今蓝色单球出现次数：")
for key in blueByNumber:
    print("数字：" + key + "，次数：" + str(blueByNumber[key]))

# 按次数升序排序（即下次概率最大到最小）
sortedRed = sorted(redByNumber.items(), key = lambda item:item[1])
sortedBlue = sorted(blueByNumber.items(), key = lambda item:item[1])
print("红球下次出现概率（数字越小概率越大）")
for i in range(0,12):
    print("数字：" + sortedRed[i][0] + "，次数：" + str(sortedRed[i][1]))

print("蓝球下次出现概率（数字越小概率越大）")
for i in range(0,6):
    print("数字：" + sortedBlue[i][0] + "，次数：" + str(sortedBlue[i][1]))

#
averageRed = count * 6 / 33
print("红球偏离量:")
for i in range(0,12):
    offset = ( averageRed - sortedRed[i][1] ) / averageRed
    print("数字：%2s" % sortedRed[i][0] + "，偏离量：%2.2f" % (offset * 100) + "%" )

averageBlue = count / 16
print("蓝球偏离量:")
for i in range(0,6):
    offset = ( averageBlue - sortedBlue[i][1] ) / averageBlue
    print("数字：%2s" % sortedBlue[i][0] + "，偏离量：%2.2f" % (offset * 100) + "%" )











