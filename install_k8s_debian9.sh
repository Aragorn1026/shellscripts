#!/bin/bash

# set -x

echo
echo
echo ++++++++++++++++++++++++++需知++++++++++++++++++++++++++
echo ++++  基于Debian 9（Debian10不可用）
echo ++++  云主机需能访问公网
echo ++++  云主机需要挂载一块磁盘（该磁盘会被格式化）
echo ++++++++++++++++++++++++++++++++++++++++++++++++++++++++
echo
echo
read -s -n1 -p "按任意键继续..."
echo

if [ $# -ne 1 ];then
    echo "Usage: ./install_k8s_debian9.sh master|worker"
    exit 1
fi

type=$1
if [ $type != master -a $type != worker ];then
    echo "Usage: ./install_k8s_debian9.sh master|worker"
    exit 1
fi

# Linux basic setting
swapoff -a
echo 'nameserver 223.5.5.5' >> /etc/resolv.conf
chattr +i /etc/resolv.conf
for i in $(seq 20 -1 0)
do
    echo -ne "Waiting for aliyun dns available: ${i}s...\r"
    sleep 1
done
echo

# Add k8s source list
echo "deb http://mirrors.ustc.edu.cn/kubernetes/apt kubernetes-xenial main" >> /etc/apt/sources.list.d/kubernetes.list
echo 'deb http://mirrors.163.com/debian/ buster main non-free contrib
deb http://mirrors.163.com/debian/ buster-updates main non-free contrib
deb http://mirrors.163.com/debian/ buster-backports main non-free contrib
deb http://mirrors.163.com/debian-security/ buster/updates main non-free contrib ' > /etc/apt/sources.list


# debian9需要先安装dirmngr，否则无法接收公钥
apt-get update &> update_result.log
apt-get install dirmngr -y

# update时会报NO PUBKEY错误，以下是解决方案
key=$(cat update_result.log | grep "GPG error" | awk '{print $NF}')
pubkey=${key:8:16}
gpg --keyserver keyserver.ubuntu.com --recv-keys $pubkey
gpg --export --armor $pubkey | apt-key add -
rm -f update_result.log

# Re-update
apt-get update

# Install docker kubeadm kubectl kubelet
apt-get install docker.io kubelet kubernetes-cni kubeadm -y

# Mount /dev/vdc
mkfs.ext4 -F /dev/vdc
mkdir /data
mount /dev/vdc /data
echo "/dev/vdc /data ext4 defaults 1 1" >> /etc/fstab

# 修改docker 数据存储目录
echo -e '{\n"graph": "/data/docker"\n}' > /etc/docker/daemon.json
mkdir /data/docker
# Restart docker
systemctl daemon-reload
systemctl restart docker
systemctl enable docker kubelet

# Debian9设置开机启动
cat > /etc/systemd/system/rc-local.service <<EOF
[Unit]
Description=/etc/rc.local
ConditionPathExists=/etc/rc.local
 
[Service]
Type=forking
ExecStart=/etc/rc.local start
TimeoutSec=0
StandardOutput=tty
RemainAfterExit=yes
SysVStartPriority=99
 
[Install]
WantedBy=multi-user.target
EOF

cat > /etc/rc.local <<EOF
#!/bin/sh
#
# rc.local
#
# This script is executed at the end of each multiuser runlevel.
# Make sure that the script will "exit 0" on success or any other
# value on error.
#
# In order to enable or disable this script just change the execution
# bits.
#
# By default this script does nothing.
exit 0
EOF

chmod +x /etc/rc.local
systemctl enable rc-local
systemctl start rc-local.service

##################################Debian9需要reboot后才可正常启动docker，以下是启动后的脚本##################################
echo '#!/bin/bash
set -x
exec &> /root/docker_pull.log

# 国外镜像无法获取，从阿里仓库获取，并tag
for image in $(kubeadm config images list | rev | cut -d / -f 1 | rev)
do
docker pull registry.cn-hangzhou.aliyuncs.com/google_containers/$image
docker tag registry.cn-hangzhou.aliyuncs.com/google_containers/$image k8s.gcr.io/$image
docker rmi registry.cn-hangzhou.aliyuncs.com/google_containers/$image
done

# 其中的coredns会拉取失败，从如下仓库拉取
docker pull coredns/coredns:1.8.0
docker tag coredns/coredns:1.8.0 k8s.gcr.io/coredns/coredns:v1.8.0
docker rmi coredns/coredns:1.8.0

# 根据节点类型，选择是否执行kubeadm init
node_type=xxxx
echo The node type is $node_type
case $node_type in
        master)
                kubeadm init
                mkdir /root/.kube
                cp /etc/kubernetes/admin.conf /root/.kube/config
                ;;
        worker)
                echo There is no init
                ;;
             *)
                echo BAD NODE TYPE, NEED TO RE-CHOOSE
                ;;
esac

# 设置cni bridge
mkdir -p /etc/cni/net.d
cat >/etc/cni/net.d/10-mynet.conf <<-EOF
{
    "cniVersion": "0.3.0",
    "name": "mynet",
    "type": "bridge",
    "bridge": "cni0",
    "isGateway": true,
    "ipMasq": true,
    "ipam": {
        "type": "host-local",
        "subnet": "10.244.0.0/16",
        "routes": [
            {"dst": "0.0.0.0/0"}
        ]
    }
}
EOF
cat >/etc/cni/net.d/99-loopback.conf <<-EOF
{
    "cniVersion": "0.3.0",
    "type": "loopback"
}
EOF

echo
echo
echo "Prompt:"
echo "节点加入集群，在master节点获取worker节点加入集群命令，复制到worker节点执行"
echo "# kubeadm token create --print-join-command "

echo "将kube config拷贝至worker节点用户目录下"
echo "# mkdir -p $HOME/.kube"
echo "# scp root@master_ip:/etc/kubernetes/admin.conf $HOME/.kube/config"
sed -i "s+/root/docker_pull.sh+#/root/docker_pull.sh+g" /etc/rc.local
rm -f /root/docker_pull.sh /root/docker_pull.log
' > /root/docker_pull.sh

# 设置重启后脚本开机自动执行
sed -i "s+xxxx+$type+g" /root/docker_pull.sh
chmod +x /root/docker_pull.sh
sed -i "s+exit 0+/root/docker_pull.sh+g" /etc/rc.local
echo "exit 0" >> /etc/rc.local

echo !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
for i in $(seq 30 -1 0)
do
    echo -ne "Will reboot after: ${i}s...\r"
    sleep 1
done
echo

reboot


