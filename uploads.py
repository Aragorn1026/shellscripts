# -*- coding: utf-8 -*-
import thread
import os
import sys
import time
import requests
import base64
import hmac
import hashlib
import platform
import json

accessKey = "xxxxxxxxx"
secretKey = "xxxxxxxxx"
bucketName = "support-nos"

osType = str(platform.platform())
if osType.startswith("Windows"):
    filePath = "C:\Users\zhangchangqing\Desktop\jpg\\"
    logPath = filePath
elif osType.startswith("Linux"):
    filePath = "/root/python-upload/jpgs/"
    logPath = "/root/python-upload/log/"
else:
    print "Unsupported OS type!"
    exit(1)

class Logger(object):

    def __init__(self, stream=sys.stdout):
        output_dir = logPath
        if not os.path.exists(output_dir):
            os.makedirs(output_dir)
        log_name = 'python-threads-upload-{}.log'.format(time.strftime('%Y-%m-%d-%H-%M-%S'))
        filename = os.path.join(output_dir, log_name)

        self.terminal = stream
        self.log = open(filename, 'a+')

    def write(self, message):
        self.terminal.write(message)
        self.log.write(message)

    def flush(self):
        pass


sys.stdout = Logger(sys.stdout)
sys.stderr = Logger(sys.stderr)


def getToken(objectName):
    putPolicy = '{"Bucket":"support-nos","Object":"' + objectName + '","Expires":1820966233, "Region": "JD"}'
    encodedPutPolicy = base64.b64encode(putPolicy)
    signature = hmac.new(secretKey, encodedPutPolicy, digestmod=hashlib.sha256).digest()
    encodedSign = base64.b64encode(signature)
    token = "UPLOAD " + accessKey + ":" + encodedSign + ":" + encodedPutPolicy
    return token


def upload(threadName):
    while True:
        for index in range(1, 300):
            localFile = filePath + str(index) + ".jpg"
            objectName = "java/" + str(index) + ".jpg"

            token = getToken(objectName)
            headers = {"Content-Type": "image/jpeg", "Content-Length": "29554", "host": "nosup-jd1.127.net", "x-nos-token":token}
            data = {"file": open(localFile, 'rb')}

            try:
                cloesestNode = requests.get("http://wanproxy.127.net/lbs?version=1.0&bucketname=support-nos")
            except Exception as e:
                print time.strftime("%Y-%m-%d %H:%M:%S", time.localtime()) + str(e)

            if index % 2 == 0:
                uploadIp = json.loads(cloesestNode.content)["upload"][0]
            else:
                uploadIp = json.loads(cloesestNode.content)["upload"][1]

            uploadUrl = "{upload_ip}/{bucket_name}/{object_name}?offset=0&complete=true&version=1.0".format(
                upload_ip=uploadIp, bucket_name=bucketName, object_name=objectName)
            try:
                res = requests.post(uploadUrl, headers=headers, data=data)
            except Exception as e:
                try:
                    time.sleep(0.5)
                    res = requests.post(uploadUrl, headers=headers, data=data)
                    print time.strftime("%Y-%m-%d %H:%M:%S", time.localtime()) + "retry successed:" + uploadUrl
                except Exception as r:
                    print time.strftime("%Y-%m-%d %H:%M:%S", time.localtime()) + "retry error: " + uploadUrl + str(r)

            print time.strftime("%Y-%m-%d %H:%M:%S", time.localtime()) + " - " + threadName + " " + res.url + ": " + str(res.status_code) + " " + res.text + " " + str(res.headers)

        time.sleep(1)


for i in range(1, 30):
    try:
        thread.start_new_thread(upload, ("thread-" + str(i),))
        time.sleep(1)
    except:
        print time.strftime("%Y-%m-%d %H:%M:%S", time.localtime()) + " Start thread error!"

while True:
    time.sleep(10)
