#!/bin/bash

# ES Address
es_url=10.0.0.2:30200
# delete indexes contain $(date) everyday 
date=$(date -d "15 day ago" +%Y.%m.%d)

index_file=/tmp/es_index_$(date +%F).txt
logfile=/tmp/deleted_es_index_$(date +%F).log

curl $es_url/_cat/indices?v > $index_file

cat $index_file | grep $date | awk '{print $3}' | while read index
do
        echo $(date +%F" "%T)" -- now will delete index: $index "
        echo curl -v -XDELETE $es_url/$index
        echo
        # sleep 1
done &> $logfile
