#!/bin/bash

logfile=cust92-checkalldisk.log
nodes=cust92-nodes
report=cust92-checkdisk-report.log
diskUsedLimit=70

# set -x
rm -f $logfile
exec 2> /dev/null

while read node
do
        echo "---- $node ----"
        ssh -o ConnectTimeout=10 $node hostname < /dev/null
        ssh -o ConnectTimeout=10 $node "df -h"  < /dev/null
        echo
        echo
done < $nodes &> $logfile

rm -f $report
exec 3>&1 1>$report

echo
echo '=========== REPORT ===========' 
echo "Disk limit is :$diskUsedLimit%" 
echo 
echo 
echo 'ERROR! Connection timed out during banner exchange' 
cat $logfile | grep 'Connection timed out' -B 2 | grep 10 | awk '{print $2}' 

while read node
do
    index=0
    result=$(cat $logfile | grep "$node" -A 20)
    echo -e "$result" | while read line
    do
        diskUsed=$(echo $line | awk '{print $5}' | sed -n 's/%//p')
        if [ $diskUsed -ge $diskUsedLimit ];then
            ((index++))
            if [ $index -eq 1 ];then
                echo
                echo "---- $node ----"
            fi
            echo $line
        fi
    done
done < $nodes 

exec 1>&3 3>&-
cat $report
