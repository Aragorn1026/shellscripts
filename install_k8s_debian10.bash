#!/bin/bash

# set -x

echo
echo
echo ++++++++++++++++++++++++++需知++++++++++++++++++++++++++
echo ++++  基于Debian 10（Debian9不可用）
echo ++++  云主机需能访问公网
echo ++++  云主机需要挂载一块磁盘（该磁盘会被格式化）
echo ++++++++++++++++++++++++++++++++++++++++++++++++++++++++
echo
echo
read -s -n1 -p "按任意键继续..."
echo

# Linux basic setting
swapoff -a
echo 'nameserver 223.5.5.5' >> /etc/resolv.conf
chattr +i /etc/resolv.conf
for i in $(seq 20 -1 0)
do
    echo -ne "Waiting for aliyun dns available: ${i}s...\r"
    sleep 1
done
echo

# add k8s source list
echo "deb http://mirrors.ustc.edu.cn/kubernetes/apt kubernetes-xenial main" >> /etc/apt/sources.list.d/kubernetes.list


# apt-get update
apt-get update &> update_result.log

# update时会报NO PUBKEY错误，以下是解决方案，原因是缺少公钥
key=$(cat update_result.log | grep "GPG error" | awk '{print $NF}')
pubkey=${key:8:16}
gpg --keyserver keyserver.ubuntu.com --recv-keys $pubkey
gpg --export --armor $pubkey | apt-key add -
rm -f update_result.log
# update
apt-get update

# 安装docker kubeadm kubectl kubelet
apt-get install docker.io kubelet kubernetes-cni kubeadm -y

# 自动挂载/dev/vdc
mkfs.ext4 -F /dev/vdc
mkdir /data
mount /dev/vdc /data
echo "/dev/vdc /data ext4 defaults 1 1" >> /etc/fstab

# 修改docker 数据存储目录
echo -e '{\n"graph": "/data/docker"\n}' > /etc/docker/daemon.json
mkdir /data/docker
# 重启docker
systemctl daemon-reload
systemctl restart docker
systemctl enable docker kubelet

# 国外镜像无法获取，从阿里仓库获取，并tag
for image in $(kubeadm config images list | awk -F '/' '{print $NF}')
do
docker pull registry.cn-hangzhou.aliyuncs.com/google_containers/$image
docker tag registry.cn-hangzhou.aliyuncs.com/google_containers/$image k8s.gcr.io/$image
docker rmi registry.cn-hangzhou.aliyuncs.com/google_containers/$image
done

# 其中的coredns会拉取失败，从如下仓库拉取
docker pull coredns/coredns:1.8.0
docker tag coredns/coredns:1.8.0 k8s.gcr.io/coredns/coredns:v1.8.4
docker rmi coredns/coredns:1.8.0

echo ++++++++++++++++++++++++++++++++++++++
echo +++++++++++choose node type+++++++++++
echo ++++++++++++++++++++++++++++++++++++++

# 根据节点类型，自主选择是否执行kubeadm init
echo "choose node type[INPUT THE INDEX]:"
select node_type in "master" "worker"
do
        echo "Your choice is $node_type"
        case $node_type in
                "master")
                        kubeadm init
                        mkdir $HOME/.kube
                        cp /etc/kubernetes/admin.conf $HOME/.kube/config
                        break ;;
                "worker")
                        echo "# There is no init"
                        break ;;
                       *)
                        echo "BAD CHOICE, NEED TO RE-CHOOSE"
                        continue ;;
        esac
done

# 设置cni bridge
mkdir -p /etc/cni/net.d
cat >/etc/cni/net.d/10-mynet.conf <<-EOF
{
    "cniVersion": "0.3.0",
    "name": "mynet",
    "type": "bridge",
    "bridge": "cni0",
    "isGateway": true,
    "ipMasq": true,
    "ipam": {
        "type": "host-local",
        "subnet": "10.244.0.0/16",
        "routes": [
            {"dst": "0.0.0.0/0"}
        ]
    }
}
EOF
cat >/etc/cni/net.d/99-loopback.conf <<-EOF
{
    "cniVersion": "0.3.0",
    "type": "loopback"
}
EOF

echo
echo
echo "Prompt:"
echo "节点加入集群，在master节点获取worker节点加入集群命令，复制到worker节点执行"
echo "# kubeadm token create --print-join-command "

echo "将kube config拷贝至worker节点用户目录下"
echo "# mkdir -p $HOME/.kube"
echo "# scp root@master_ip:/etc/kubernetes/admin.conf $HOME/.kube/config"

