# -*- coding: utf-8 -*-

import base64
import hmac
import hashlib
import urllib
import json
import platform

bucketName = "support-nos"
accessKey = "xxxxxxxxx"
secretKey = "xxxxxxxxx"
putPolicy = '{"Bucket":"support-nos","Object":"java/1.jpg","Expires":1820966233, "Region": "JD"}'

#签名字符串
encodedPutPolicy = base64.b64encode(putPolicy)

#对签名字符串计算HMAC-SHA256签名
signature = hmac.new(secretKey, encodedPutPolicy, digestmod=hashlib.sha256).digest()

#对签名进行Base64编码
encodedSign = base64.b64encode(signature)
token = "UPLOAD " + accessKey + ":" + encodedSign + ":" + encodedPutPolicy
print token

print platform.node()
print platform.platform()

if str(platform.platform()).startswith("Windows"):
    print "Windows!"
elif str(platform.platform()).startswith("Linux"):
    print "Linux!"
else:
    print "Others"

